# backendSr
Test backendSr
## _Script Base de Datos - gestor: MySQL_
```
DROP DATABASE IF EXISTS apimarvel;
CREATE DATABASE apimarvel;
USE apimarvel;
DROP TABLE IF EXISTS characters;
CREATE TABLE characters (
	id INT NOT NULL PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    descripcion LONGTEXT NULL,
    imagen LONGTEXT NULL,
    comics LONGTEXT NULL,
    series LONGTEXT NULL,
    created_at DATE NOT NULL,
    updated_at DATE NULL
);

DROP TABLE IF EXISTS comics;
CREATE TABLE comics (
	id INT NOT NULL PRIMARY KEY,
    titulo LONGTEXT NOT NULL,
    descripcion LONGTEXT NULL,
    creador LONGTEXT NULL,
    created_at DATE NOT NULL,
    updated_at DATE NULL
);

DROP TABLE IF EXISTS creators;
CREATE TABLE creators (
	id INT NOT NULL PRIMARY KEY,
    nombre LONGTEXT NULL,
    apellido LONGTEXT NULL,
    nomcompleto LONGTEXT NULL,
    comics LONGTEXT NULL,
    created_at DATE NOT NULL,
    updated_at DATE NULL
);

DROP TABLE IF EXISTS usuarios;
CREATE TABLE usuarios (
	idusr VARCHAR(50) NOT NULL PRIMARY KEY,
    pass LONGTEXT NOT NULL,
    nombres VARCHAR(250) NULL,
    created_at DATE NOT NULL
);

DROP TABLE IF EXISTS historial;
CREATE TABLE historial (
	idhistorial DATE NOT NULL PRIMARY KEY,
    idusr VARCHAR(50) NOT NULL,
    tipobusqueda VARCHAR(75) NOT NULL,
    contbusqueda LONGTEXT NOT NULL,
    resultbusqueda LONGTEXT NOT NULL,
    FOREIGN KEY (idusr) REFERENCES usuarios(idusr)
);
```

## _Configuración ambiente Docker_
*Dockerfile - consumer-api-marvel*
```
FROM gradle:jdk11 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon

FROM openjdk:11-jre-slim
EXPOSE 8081
RUN mkdir /app
#
COPY --from=build /home/gradle/src/build/libs/consumer-api-marvel.jar consumer-api-marvel.jar
ENTRYPOINT ["java", "-Dspring.profiles.active=dev", "-jar", "consumer-api-marvel.jar"]
```

*Construcción imagen*
Ubicarse en la raíz del proyecto y ejecutar el comando:
```
docker build -t consumer-api-marvel .
```

*Dockerfile - informacion-marvel*
```
FROM gradle:jdk11 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon

FROM openjdk:11-jre-slim
EXPOSE 8081
RUN mkdir /app
#
COPY --from=build /home/gradle/src/build/libs/informacion-marvel.jar informacion-marvel.jar
ENTRYPOINT ["java", "-Dspring.profiles.active=dev", "-jar", "informacion-marvel.jar"]
```

*Construcción imagen*
Ubicarse en la raíz del proyecto y ejecutar el comando:
```
docker build -t informacion-marvel .
```

*Red Docker*
```
docker network create --driver=overlay --attachable microsrv
```
*Servicio MySQL en la red microsv*
```
docker service create --name mysql --hostname mysql -e MYSQL_ROOT_PASSWORD=admin -e TZ=America/El_Salvador --restart-condition on-failure --network microsrv --limit-memory 5G -p 7707:3306 mysql:latest
```
*Servicio consumer-api-marvel en la red microsv*
```
docker service create --name consumer-api-marvel --hostname consumer-api-marvel -e TZ=America/El_Salvador --restart-condition on-failure --network microsrv --limit-memory 5G -p 8080:8080 consumer-api-marvel:1217
```
*Servicio informacion-marvel en la red microsv*
```
docker service create --name informacion-marvel --hostname informacion-marvel -e TZ=America/El_Salvador --restart-condition on-failure --network microsrv --limit-memory 5G -p 8081:8081 informacion-marvel:1217
```
