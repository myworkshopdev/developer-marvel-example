package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerApiMarvelApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumerApiMarvelApplication.class, args);
	}

}
