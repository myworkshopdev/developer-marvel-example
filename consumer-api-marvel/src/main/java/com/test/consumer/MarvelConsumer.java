package com.test.consumer;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.test.dto.CharactersMarvelReq;
import com.test.exceptions.AppServiceException;
import com.test.exceptions.ErrorMessage;
import com.test.model.Character;
import com.test.model.Comic;
import com.test.model.Creator;
import com.test.model.Series;
import com.test.notifications.CtsCodeMsg;
import com.test.util.AppUtil;
import com.test.util.JSONUtils;

@Component
public class MarvelConsumer {
	private static final Logger log = LoggerFactory.getLogger(MarvelConsumer.class);
	private static final HttpClient client = HttpClient.newBuilder().version(Version.HTTP_2).build();
	
	@Autowired
	private AppUtil appUtil;
	
	public Character charactersData(CharactersMarvelReq cmReq) {
		ErrorMessage er = new ErrorMessage();
		
		try {
			log.info("Proceso de consulta personaje Marvel por nombre, comics y series");
			
			String charactersURL = CtsCodeMsg.BASE_URL + CtsCodeMsg.CHARACTERS_URL;
			HttpRequest httpReq = HttpRequest.newBuilder(URI.create(appUtil.buildURL(charactersURL, cmReq))).GET().build();
			CompletableFuture<HttpResponse<String>> response = client.sendAsync(httpReq, BodyHandlers.ofString());
			
			if (response.get().statusCode() == 200) {
				JSONObject body = new JSONObject(response.get().body());
				JSONObject data = body.getJSONObject("data");
				JSONArray results = data.getJSONArray("results");
				
				if ((int) data.get("total") > 0) {
					return JSONUtils.convertFromJsonToObject(results.get(0).toString(), Character.class);
				} else {
					er.setECode(Long.valueOf(HttpStatus.NOT_FOUND.value()));
					er.setErrorMessage("No hubo coincidencias");
					log.error("Causa del problema: {}", er);
					
					throw new AppServiceException(er);
				}
			} else {
				er.setECode(Long.valueOf(response.get().statusCode()));
				er.setErrorMessage("La consulta de personajes Marvel fue fallida");
				log.error("Causa del problema: {}", er);
				
				throw new AppServiceException(er);
			}
		} catch (AppServiceException e) {
			throw e;
		} catch (InterruptedException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("InterruptedException: {}", er);
			
			throw new AppServiceException(er);
		} catch (ExecutionException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("ExecutionException: {}", er);
			
			throw new AppServiceException(er);
		}
	}

	public List<Map<String, Object>> characterComicsData(int idCharacter) {
		List<Map<String, Object>> rspComicList = new ArrayList<Map<String, Object>>();
		
		ErrorMessage er = new ErrorMessage();
		
		try {
			log.info("Proceso de consulta de comics por id personaje Marvel");
			
			String charactersURL = String.format(CtsCodeMsg.CHARACTERS_COMICS_URL, idCharacter);
			HttpRequest httpReq = HttpRequest.newBuilder(URI.create(appUtil.buildURL(charactersURL))).GET().build();
			CompletableFuture<HttpResponse<String>> response = client.sendAsync(httpReq, BodyHandlers.ofString());
			
			if (response.get().statusCode() == 200) {
				JSONObject body = new JSONObject(response.get().body());
				JSONObject data = body.getJSONObject("data");
				
				int offset = 0;
				int total = (int) data.get("total");
				
				while (total > offset) {
					JSONArray results = data.getJSONArray("results");
				
					for (int i = 0; i < results.length(); i++) {
						Map<String, Object> rspComicMap = new HashMap<String, Object>();
						JSONObject result = results.getJSONObject(i);
						Comic comic = JSONUtils.convertFromJsonToObject(result.toString(), Comic.class);

						rspComicMap.put("id", comic.getId());
						rspComicMap.put("titulo", comic.getTitle());
						rspComicMap.put("descripcion", comic.getDescription());
						rspComicMap.put("creador", comicCreatorsData(comic.getId()));
						
						rspComicList.add(rspComicMap);
						
						offset++;
					}

					if (total > offset) {
						httpReq = HttpRequest.newBuilder(URI.create(appUtil.buildURL(charactersURL, offset))).GET().build();
						response = client.sendAsync(httpReq, BodyHandlers.ofString());
						
						body = new JSONObject(response.get().body());
						data = body.getJSONObject("data");
					}
				}
				
				return rspComicList;
			} else {
				er.setECode(Long.valueOf(response.get().statusCode()));
				er.setErrorMessage("La consulta de personajes Marvel fue fallida");
				log.error("Causa del problema: {}", er);
				
				throw new AppServiceException(er);
			}
		} catch (AppServiceException e) {
			throw e;
		} catch (InterruptedException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("InterruptedException: {}", er);
			
			throw new AppServiceException(er);
		} catch (ExecutionException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("ExecutionException: {}", er);
			
			throw new AppServiceException(er);
		}
	}

	public List<Map<String, Object>> creatorComicsData(int idCreator) {
		List<Map<String, Object>> rspComicList = new ArrayList<Map<String, Object>>();
		
		ErrorMessage er = new ErrorMessage();
		
		try {
			log.info("Proceso de consulta de comics por id personaje Marvel");
			
			String charactersURL = String.format(CtsCodeMsg.CREATORS_COMICS_URL, idCreator);
			HttpRequest httpReq = HttpRequest.newBuilder(URI.create(appUtil.buildURL(charactersURL))).GET().build();
			CompletableFuture<HttpResponse<String>> response = client.sendAsync(httpReq, BodyHandlers.ofString());
			
			if (response.get().statusCode() == 200) {
				JSONObject body = new JSONObject(response.get().body());
				JSONObject data = body.getJSONObject("data");
				
				int offset = 0;
				int total = (int) data.get("total");
				
				while (total > offset) {
					JSONArray results = data.getJSONArray("results");
				
					for (int i = 0; i < results.length(); i++) {
						Map<String, Object> rspComicMap = new HashMap<String, Object>();
						JSONObject result = results.getJSONObject(i);
						Comic comic = JSONUtils.convertFromJsonToObject(result.toString(), Comic.class);

						rspComicMap.put("id", comic.getId());
						rspComicMap.put("titulo", comic.getTitle());
						
						rspComicList.add(rspComicMap);
						
						offset++;
					}

					if (total > offset) {
						httpReq = HttpRequest.newBuilder(URI.create(appUtil.buildURL(charactersURL, offset))).GET().build();
						response = client.sendAsync(httpReq, BodyHandlers.ofString());
						
						body = new JSONObject(response.get().body());
						data = body.getJSONObject("data");
					}
				}
				
				return rspComicList;
			} else {
				er.setECode(Long.valueOf(response.get().statusCode()));
				er.setErrorMessage("La consulta de personajes Marvel fue fallida");
				log.error("Causa del problema: {}", er);
				
				throw new AppServiceException(er);
			}
		} catch (AppServiceException e) {
			throw e;
		} catch (InterruptedException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("InterruptedException: {}", er);
			
			throw new AppServiceException(er);
		} catch (ExecutionException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("ExecutionException: {}", er);
			
			throw new AppServiceException(er);
		}
	}

	public Map<String, Object> comicCreatorsData(int idComic) {
		List<Map<String, Object>> rspCreatorList = new ArrayList<Map<String, Object>>();
		
		ErrorMessage er = new ErrorMessage();
		
		try {
			log.info("Proceso de consulta de creadores por id comics de Marvel");
			
			String comicCreatorsURL = String.format(CtsCodeMsg.COMICS_CREATORS_URL, idComic);
			HttpRequest httpReq = HttpRequest.newBuilder(URI.create(appUtil.buildURL(comicCreatorsURL))).GET().build();
			CompletableFuture<HttpResponse<String>> response = client.sendAsync(httpReq, BodyHandlers.ofString());
			
			if (response.get().statusCode() == 200) {
				JSONObject body = new JSONObject(response.get().body());
				JSONObject data = body.getJSONObject("data");
				
				int offset = 0;
				int total = (int) data.get("total");
				
				while (total > offset) {
					JSONArray results = data.getJSONArray("results");
				
					for (int i = 0; i < results.length(); i++) {
						Map<String, Object> rspCreatorMap = new HashMap<String, Object>();
						JSONObject result = results.getJSONObject(i);
						Creator creator = JSONUtils.convertFromJsonToObject(result.toString(), Creator.class);

						rspCreatorMap.put("id", creator.getId());
						rspCreatorMap.put("nombreCompleto", creator.getFullName());
						
						rspCreatorList.add(rspCreatorMap);
						
						offset++;
					}

					if (total > offset) {
						httpReq = HttpRequest.newBuilder(URI.create(appUtil.buildURL(comicCreatorsURL, offset))).GET().build();
						response = client.sendAsync(httpReq, BodyHandlers.ofString());
						
						body = new JSONObject(response.get().body());
						data = body.getJSONObject("data");
					}
				}
				
				Map<String, Object> infoCollection = new HashMap<String, Object>();
				infoCollection.put("disponibles", total);
				infoCollection.put("urlColeccion", comicCreatorsURL);
				infoCollection.put("items", rspCreatorList);
				return infoCollection;
			} else {
				er.setECode(Long.valueOf(response.get().statusCode()));
				er.setErrorMessage("La consulta de personajes Marvel fue fallida");
				log.error("Causa del problema: {}", er);
				
				throw new AppServiceException(er);
			}
		} catch (AppServiceException e) {
			throw e;
		} catch (InterruptedException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("InterruptedException: {}", er);
			
			throw new AppServiceException(er);
		} catch (ExecutionException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("ExecutionException: {}", er);
			
			throw new AppServiceException(er);
		}
	}

	public List<Map<String, Object>> seriesData(int idCharacter) {
		List<Map<String, Object>> rspComicList = new ArrayList<Map<String, Object>>();
		
		ErrorMessage er = new ErrorMessage();
		
		try {
			log.info("Proceso de consulta de series por id personaje Marvel");
			
			String charactersURL = String.format(CtsCodeMsg.CHARACTERS_SERIES_URL, idCharacter);
			HttpRequest httpReq = HttpRequest.newBuilder(URI.create(appUtil.buildURL(charactersURL))).GET().build();
			CompletableFuture<HttpResponse<String>> response = client.sendAsync(httpReq, BodyHandlers.ofString());
			
			if (response.get().statusCode() == 200) {
				JSONObject body = new JSONObject(response.get().body());
				JSONObject data = body.getJSONObject("data");
				
				int offset = 0;
				int total = (int) data.get("total");
				
				while (total > offset) {
					JSONArray results = data.getJSONArray("results");
				
					for (int i = 0; i < results.length(); i++) {
						Map<String, Object> rspComicMap = new HashMap<String, Object>();
						JSONObject result = results.getJSONObject(i);
						Series series = JSONUtils.convertFromJsonToObject(result.toString(), Series.class);

						rspComicMap.put("id", series.getId());
						rspComicMap.put("titulo", series.getTitle());
						rspComicMap.put("descripcion", series.getDescription());
						rspComicMap.put("creador", series.getCreators());
						
						rspComicList.add(rspComicMap);
					
						offset++;
					}

					if (total > offset) {
						httpReq = HttpRequest.newBuilder(URI.create(appUtil.buildURL(charactersURL, offset))).GET().build();
						response = client.sendAsync(httpReq, BodyHandlers.ofString());
						
						body = new JSONObject(response.get().body());
						data = body.getJSONObject("data");
					}
				}
				
				return rspComicList;
			} else {
				er.setECode(Long.valueOf(response.get().statusCode()));
				er.setErrorMessage("La consulta de personajes Marvel fue fallida");
				log.error("Causa del problema: {}", er);
				
				throw new AppServiceException(er);
			}
		} catch (AppServiceException e) {
			throw e;
		} catch (InterruptedException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("InterruptedException: {}", er);
			
			throw new AppServiceException(er);
		} catch (ExecutionException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("ExecutionException: {}", er);
			
			throw new AppServiceException(er);
		}
	}

	public Comic comicsData(int idComic) {
		ErrorMessage er = new ErrorMessage();
		
		try {
			log.info("Proceso de consulta comics Marvel por id");
			
			String comicsURL = String.format(CtsCodeMsg.COMICS_BY_ID_URL, idComic);
			HttpRequest httpReq = HttpRequest.newBuilder(URI.create(appUtil.buildURL(comicsURL))).GET().build();
			CompletableFuture<HttpResponse<String>> response = client.sendAsync(httpReq, BodyHandlers.ofString());
			
			if (response.get().statusCode() == 200) {
				JSONObject body = new JSONObject(response.get().body());
				JSONObject data = body.getJSONObject("data");
				JSONArray results = data.getJSONArray("results");
				
				return JSONUtils.convertFromJsonToObject(results.get(0).toString(), Comic.class);
			} else {
				er.setECode(Long.valueOf(response.get().statusCode()));
				er.setErrorMessage("La consulta de personajes Marvel fue fallida");
				log.error("Causa del problema: {}", er);
				
				throw new AppServiceException(er);
			}
		} catch (AppServiceException e) {
			throw e;
		} catch (InterruptedException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("InterruptedException: {}", er);
			
			throw new AppServiceException(er);
		} catch (ExecutionException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("ExecutionException: {}", er);
			
			throw new AppServiceException(er);
		}
	}

	public List<Creator> creatorsData(String firstName) {
		List<Creator> creatorList = new ArrayList<Creator>();
		ErrorMessage er = new ErrorMessage();
		
		try {
			log.info("Proceso de consulta de creadores de Marvel");
			
			HttpRequest httpReq = HttpRequest.newBuilder(URI.create(appUtil.buildURL(CtsCodeMsg.CREATORS_URL, firstName))).GET().build();
			CompletableFuture<HttpResponse<String>> response = client.sendAsync(httpReq, BodyHandlers.ofString());
			
			if (response.get().statusCode() == 200) {
				JSONObject body = new JSONObject(response.get().body());
				JSONObject data = body.getJSONObject("data");

				int offset = 0;
				int total = (int) data.get("total");
				
				while (total > offset) {
					JSONArray results = data.getJSONArray("results");
				
					for (int i = 0; i < results.length(); i++) {
						JSONObject result = results.getJSONObject(i);
						Creator creator = JSONUtils.convertFromJsonToObject(result.toString(), Creator.class);
						creatorList.add(creator);
						offset++;
					}

					if (total > offset) {
						httpReq = HttpRequest.newBuilder(URI.create(appUtil.buildURL(CtsCodeMsg.CREATORS_URL, firstName, offset))).GET().build();
						response = client.sendAsync(httpReq, BodyHandlers.ofString());
						
						body = new JSONObject(response.get().body());
						data = body.getJSONObject("data");
					}
				}
				
				return creatorList;
			} else {
				er.setECode(Long.valueOf(response.get().statusCode()));
				er.setErrorMessage("La consulta de creadores Marvel fue fallida");
				log.error("Causa del problema: {}", er);
				
				throw new AppServiceException(er);
			}
		} catch (AppServiceException e) {
			throw e;
		} catch (InterruptedException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("InterruptedException: {}", er);
			
			throw new AppServiceException(er);
		} catch (ExecutionException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("ExecutionException: {}", er);
			
			throw new AppServiceException(er);
		}
	}

}
