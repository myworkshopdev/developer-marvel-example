package com.test.model;

import java.util.Arrays;

public class CharacterList extends AbstractList {
	private static final long serialVersionUID = 6767157928035526452L;

	private CharacterSummary[] items;

	public CharacterList() {
		super();
	}

	public CharacterList(int available, int returned, String collectionURI, CharacterSummary[] items) {
		super(available, returned, collectionURI);
		this.items = items;
	}

	public CharacterSummary[] getItems() {
		return items;
	}

	public void setItems(CharacterSummary[] items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "CharacterList [items=" + Arrays.toString(items) + "]";
	}

}
