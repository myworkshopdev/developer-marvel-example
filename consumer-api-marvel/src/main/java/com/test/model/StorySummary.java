package com.test.model;

public class StorySummary extends AbstractSummary {
	private static final long serialVersionUID = 7873967344086584317L;
	
	private String type;

	public StorySummary() {
		super();
	}

	public StorySummary(String resourceURI, String name, String type) {
		super(resourceURI, name);
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "StorySummary [type=" + type + "]";
	}

}
