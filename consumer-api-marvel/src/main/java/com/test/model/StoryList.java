package com.test.model;

import java.util.Arrays;

public class StoryList extends AbstractList {
	private static final long serialVersionUID = 1856340392901645594L;
	
	private StorySummary[] items;

	public StoryList() {
		super();
	}

	public StoryList(int available, int returned, String collectionURI, StorySummary[] items) {
		super(available, returned, collectionURI);
		this.items = items;
	}

	public StorySummary[] getItems() {
		return items;
	}

	public void setItems(StorySummary[] items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "StoryList [items=" + Arrays.toString(items) + "]";
	}

}
