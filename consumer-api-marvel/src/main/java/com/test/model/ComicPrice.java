package com.test.model;

import java.io.Serializable;

public class ComicPrice implements Serializable {
	private static final long serialVersionUID = -4644920975076123038L;
	
	private String type;
	private float price;

	public ComicPrice() {
		super();
	}

	public ComicPrice(String type, float price) {
		super();
		this.type = type;
		this.price = price;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "ComicPrice [type=" + type + ", price=" + price + "]";
	}

}
