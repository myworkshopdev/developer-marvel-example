package com.test.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

public class Character implements Serializable {
	private static final long serialVersionUID = 4549822620677900369L;
	
	private int id;
	private String name;
	private String description;
	private Date modified;
	private String resourceURI;
	private Url[] urls;
	private Image thumbnail;
	private ComicList comics;
	private StoryList stories;
	private EventList events;
	private SeriesList series;
	
	public Character() {
		super();
	}

	public Character(int id, String name, String description, Date modified, String resourceURI, Url[] urls,
			Image thumbnail, ComicList comics, StoryList stories, EventList events, SeriesList series) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.modified = modified;
		this.resourceURI = resourceURI;
		this.urls = urls;
		this.thumbnail = thumbnail;
		this.comics = comics;
		this.stories = stories;
		this.events = events;
		this.series = series;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public String getResourceURI() {
		return resourceURI;
	}

	public void setResourceURI(String resourceURI) {
		this.resourceURI = resourceURI;
	}

	public Url[] getUrls() {
		return urls;
	}

	public void setUrls(Url[] urls) {
		this.urls = urls;
	}

	public Image getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(Image thumbnail) {
		this.thumbnail = thumbnail;
	}

	public ComicList getComics() {
		return comics;
	}

	public void setComics(ComicList comics) {
		this.comics = comics;
	}

	public StoryList getStories() {
		return stories;
	}

	public void setStories(StoryList stories) {
		this.stories = stories;
	}

	public EventList getEvents() {
		return events;
	}

	public void setEvents(EventList events) {
		this.events = events;
	}

	public SeriesList getSeries() {
		return series;
	}

	public void setSeries(SeriesList series) {
		this.series = series;
	}

	@Override
	public String toString() {
		return "Characters [id=" + id + ", name=" + name + ", description=" + description + ", modified=" + modified
				+ ", resourceURI=" + resourceURI + ", urls=" + Arrays.toString(urls) + ", thumbnail=" + thumbnail
				+ ", comics=" + comics + ", stories=" + stories + ", events=" + events + ", series=" + series + "]";
	}
	
}
