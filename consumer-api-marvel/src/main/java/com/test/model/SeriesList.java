package com.test.model;

import java.util.Arrays;

public class SeriesList extends AbstractList {
	private static final long serialVersionUID = -1442218448497152639L;

	private SeriesSummary[] items;

	public SeriesList() {
		super();
	}

	public SeriesList(int available, int returned, String collectionURI, SeriesSummary[] items) {
		super(available, returned, collectionURI);
		this.items = items;
	}

	public SeriesSummary[] getItems() {
		return items;
	}

	public void setItems(SeriesSummary[] items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "SeriesList [items=" + Arrays.toString(items) + "]";
	}

}
