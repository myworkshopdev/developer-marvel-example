package com.test.model;

import java.io.Serializable;

public abstract class AbstractSummary implements Serializable {
	private static final long serialVersionUID = 7050270190190889927L;

	protected String resourceURI;
	protected String name;

	public AbstractSummary() {
		super();
	}

	public AbstractSummary(String resourceURI, String name) {
		super();
		this.resourceURI = resourceURI;
		this.name = name;
	}

	public String getResourceURI() {
		return resourceURI;
	}

	public void setResourceURI(String resourceURI) {
		this.resourceURI = resourceURI;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "AbstratSummary [resourceURI=" + resourceURI + ", name=" + name + "]";
	}

}
