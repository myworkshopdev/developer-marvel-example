package com.test.model;

import java.io.Serializable;
import java.util.Arrays;

public class Creator implements Serializable {
	private static final long serialVersionUID = -5326644008355377466L;

	private int id;
	private String firstName;
	private String middleName;
	private String lastName;
	private String suffix;
	private String fullName;
	private String modified;
	private String resourceURI;
	private Url[] urls;
	private Image thumbnail;
	private SeriesList series;
	private StoryList stories;
	private ComicList comics;
	private EventList events;

	public Creator() {
		super();
	}

	public Creator(int id, String firstName, String middleName, String lastName, String suffix, String fullName,
			String modified, String resourceURI, Url[] urls, Image thumbnail, SeriesList series, StoryList stories,
			ComicList comics, EventList events) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.suffix = suffix;
		this.fullName = fullName;
		this.modified = modified;
		this.resourceURI = resourceURI;
		this.urls = urls;
		this.thumbnail = thumbnail;
		this.series = series;
		this.stories = stories;
		this.comics = comics;
		this.events = events;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public String getResourceURI() {
		return resourceURI;
	}

	public void setResourceURI(String resourceURI) {
		this.resourceURI = resourceURI;
	}

	public Url[] getUrls() {
		return urls;
	}

	public void setUrls(Url[] urls) {
		this.urls = urls;
	}

	public Image getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(Image thumbnail) {
		this.thumbnail = thumbnail;
	}

	public SeriesList getSeries() {
		return series;
	}

	public void setSeries(SeriesList series) {
		this.series = series;
	}

	public StoryList getStories() {
		return stories;
	}

	public void setStories(StoryList stories) {
		this.stories = stories;
	}

	public ComicList getComics() {
		return comics;
	}

	public void setComics(ComicList comics) {
		this.comics = comics;
	}

	public EventList getEvents() {
		return events;
	}

	public void setEvents(EventList events) {
		this.events = events;
	}

	@Override
	public String toString() {
		return "Creators [id=" + id + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName="
				+ lastName + ", suffix=" + suffix + ", fullName=" + fullName + ", modified=" + modified
				+ ", resourceURI=" + resourceURI + ", urls=" + Arrays.toString(urls) + ", thumbnail=" + thumbnail
				+ ", series=" + series + ", stories=" + stories + ", comics=" + comics + ", events=" + events + "]";
	}

}
