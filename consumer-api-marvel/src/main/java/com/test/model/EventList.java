package com.test.model;

import java.util.Arrays;

public class EventList extends AbstractList {
	private static final long serialVersionUID = 2923965139720782826L;
	
	private EventSummary[] items;

	public EventList() {
		super();
	}

	public EventList(int available, int returned, String collectionURI, EventSummary[] items) {
		super(available, returned, collectionURI);
		this.items = items;
	}

	public EventSummary[] getItems() {
		return items;
	}

	public void setItems(EventSummary[] items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "EventList [available=" + available + ", returned=" + returned + ", collectionURI=" + collectionURI
				+ ", items=" + Arrays.toString(items) + "]";
	}

}
