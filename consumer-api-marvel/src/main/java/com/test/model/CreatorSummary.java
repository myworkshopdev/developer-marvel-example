package com.test.model;

public class CreatorSummary extends AbstractSummary {
	private static final long serialVersionUID = 3764128283404911504L;

	private String role;

	public CreatorSummary() {
		super();
	}

	public CreatorSummary(String resourceURI, String name, String role) {
		super(resourceURI, name);
		this.role = role;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "CreatorSummary [role=" + role + "]";
	}

}
