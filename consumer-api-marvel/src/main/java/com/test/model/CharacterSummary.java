package com.test.model;

public class CharacterSummary extends AbstractSummary {
	private static final long serialVersionUID = 6976367961801155393L;

	private String role;

	public CharacterSummary() {
		super();
	}

	public CharacterSummary(String resourceURI, String name, String role) {
		super(resourceURI, name);
		this.role = role;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "CreatorSummary [role=" + role + "]";
	}

}
