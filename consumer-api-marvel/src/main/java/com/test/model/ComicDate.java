package com.test.model;

import java.io.Serializable;

public class ComicDate implements Serializable {
	private static final long serialVersionUID = 8485181057855286712L;
	
	private String type;
	private String date;

	public ComicDate() {
		super();
	}

	public ComicDate(String type, String date) {
		super();
		this.type = type;
		this.date = date;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "ComicDate [type=" + type + ", date=" + date + "]";
	}

}
