package com.test.model;

import java.util.Arrays;

public class ComicList extends AbstractList {
	private static final long serialVersionUID = -1240959702054158075L;
	
	private ComicSummary[] items;

	public ComicList() {
		super();
	}

	public ComicList(int available, int returned, String collectionURI, ComicSummary[] items) {
		super(available, returned, collectionURI);
		this.items = items;
	}

	public ComicSummary[] getItems() {
		return items;
	}

	public void setItems(ComicSummary[] items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "ComicList [available=" + available + ", returned=" + returned + ", collectionURI=" + collectionURI
				+ ", items=" + Arrays.toString(items) + "]";
	}

}
