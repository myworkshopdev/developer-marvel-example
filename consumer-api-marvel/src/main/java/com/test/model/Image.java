package com.test.model;

import java.io.Serializable;

public class Image implements Serializable {
	private static final long serialVersionUID = 7776747188337950211L;
	
	private String path;
	private String extension;

	public Image() {
		super();
	}

	public Image(String path, String extension) {
		super();
		this.path = path;
		this.extension = extension;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	@Override
	public String toString() {
		return "Image [path=" + path + ", extension=" + extension + "]";
	}

}
