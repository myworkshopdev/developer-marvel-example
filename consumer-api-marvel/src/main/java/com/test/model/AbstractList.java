package com.test.model;

import java.io.Serializable;

public abstract class AbstractList implements Serializable {
	private static final long serialVersionUID = -3314417995397634444L;
	
	protected int available;
	protected int returned;
	protected String collectionURI;

	public AbstractList() {
		super();
	}

	public AbstractList(int available, int returned, String collectionURI) {
		super();
		this.available = available;
		this.returned = returned;
		this.collectionURI = collectionURI;
	}

	public int getAvailable() {
		return available;
	}

	public void setAvailable(int available) {
		this.available = available;
	}

	public int getReturned() {
		return returned;
	}

	public void setReturned(int returned) {
		this.returned = returned;
	}

	public String getCollectionURI() {
		return collectionURI;
	}

	public void setCollectionURI(String collectionURI) {
		this.collectionURI = collectionURI;
	}

}
