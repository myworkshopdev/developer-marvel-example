package com.test.model;

import java.util.Arrays;

public class CreatorList extends AbstractList {
	private static final long serialVersionUID = -2005084370331156732L;
	
	private CreatorSummary[] items;

	public CreatorList() {
		super();
	}

	public CreatorList(int available, int returned, String collectionURI, CreatorSummary[] items) {
		super(available, returned, collectionURI);
		this.items = items;
	}

	public CreatorSummary[] getItems() {
		return items;
	}

	public void setItems(CreatorSummary[] items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "CreatorList [items=" + Arrays.toString(items) + "]";
	}
	
}
