package com.test.model;

import java.io.Serializable;

public class Url implements Serializable {
	private static final long serialVersionUID = -4153266389057856071L;
	
	private String type;
	private String url;

	public Url() {
		super();
	}

	public Url(String type, String url) {
		super();
		this.type = type;
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "Url [type=" + type + ", url=" + url + "]";
	}

}
