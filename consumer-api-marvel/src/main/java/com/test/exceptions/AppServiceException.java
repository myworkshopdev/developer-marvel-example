package com.test.exceptions;

public class AppServiceException extends RuntimeException {
	private static final long serialVersionUID = -6657933755667643856L;
	
	private final ErrorMessage error;

	public AppServiceException(ErrorMessage error) {
		super(error.getErrorMessage());
		
		this.error = error;
	}

	public ErrorMessage getError() {
		return error;
	}

}
