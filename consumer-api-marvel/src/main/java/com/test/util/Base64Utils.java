package com.test.util;

import java.util.Base64;

public class Base64Utils {
	
	public Base64Utils() {
		super();
	}

	// Basic Encoding & Decoding
	public static String encodePhrase(String phrase) {
		return Base64.getEncoder().encodeToString(phrase.getBytes());
	}
	
	public static String decodePhrase(String phrase) {
		return new String(Base64.getDecoder().decode(phrase));
	}
	
	// URL Encoding & Decoding 
	public static String encodeURL(String url) {
		return Base64.getUrlEncoder().encodeToString(url.getBytes());
	}
	
	public static String decodeURL(String url) {
		return new String(Base64.getUrlDecoder().decode(url));
	}
}
