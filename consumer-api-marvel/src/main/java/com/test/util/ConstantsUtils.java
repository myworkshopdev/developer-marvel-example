package com.test.util;

public class ConstantsUtils {
	
	private ConstantsUtils() {}
	
	public static final Long CODE_SUCCESS_KEY = Long.valueOf(0L);
	public static final String CODE_SUCCESS_VALUE = "OK";
	public static final String CODE_FAILED_VALUE = "ERROR";
	public static final String SERVICE_CODE = "service-code";
	public static final String SERVICE_DESCRIPTION = "service-description";
	public static final String X_USER_AGENT_KEY = "x-user-agent";
	public static final String X_USER_AGENT_VALUE = "Prueba Tecnica - Banco Cuscatlan S.A.";
	public static final String OPERATION_REF_ID = "operation-reference-id";
	public static final String UNTIL_LAST_SEQUENCE = "until-last-sequence";
	public static final String LOGGING_TRACE_SPLIT = "\\:";
	public static final String GATEWAY_VALUE = "Gateway";
	public static final String HEADERS_VALIDATION_RESULT = "Headers validation result:";
	public static final String REQ_HEADERS_NOT_FOUND = "Request headers not found";
}
