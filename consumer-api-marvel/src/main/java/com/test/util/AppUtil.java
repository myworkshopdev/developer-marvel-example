package com.test.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

import com.test.dto.CharactersMarvelReq;
import com.test.notifications.CtsCodeMsg;

@Component
public class AppUtil {
	public String buildURL(String urlService, CharactersMarvelReq req) {
		StringBuilder url = new StringBuilder(urlService);
		
		Long ts = System.currentTimeMillis();
		
		url.append("?name=") 
		.append(req.getName().replace(" ", "%20"))
		.append("&comics=")
		.append(req.getComics())
		.append("&series=")
		.append(req.getSeries())
		.append("&ts=")
		.append(ts)
		.append("&apikey=")
		.append(Base64Utils.decodePhrase(CtsCodeMsg.PUBLIC_KEY))
		.append("&hash=")
		.append(generarHash(ts, Base64Utils.decodePhrase(CtsCodeMsg.PRIVATE_KEY), Base64Utils.decodePhrase(CtsCodeMsg.PUBLIC_KEY)));
		
		return url.toString();
	}

	public String buildURL(String urlService, String firstName) {
		StringBuilder url = new StringBuilder(urlService);
		
		Long ts = System.currentTimeMillis();
		
		url.append("?firstName=") 
		.append(firstName)
		.append("&ts=")
		.append(ts)
		.append("&apikey=")
		.append(Base64Utils.decodePhrase(CtsCodeMsg.PUBLIC_KEY))
		.append("&hash=")
		.append(generarHash(ts, Base64Utils.decodePhrase(CtsCodeMsg.PRIVATE_KEY), Base64Utils.decodePhrase(CtsCodeMsg.PUBLIC_KEY)));
		
		return url.toString();
	}

	public String buildURL(String urlService) {
		StringBuilder url = new StringBuilder(urlService);
		
		Long ts = System.currentTimeMillis();
		
		url.append("?ts=")
		.append(ts)
		.append("&apikey=")
		.append(Base64Utils.decodePhrase(CtsCodeMsg.PUBLIC_KEY))
		.append("&hash=")
		.append(generarHash(ts, Base64Utils.decodePhrase(CtsCodeMsg.PRIVATE_KEY), Base64Utils.decodePhrase(CtsCodeMsg.PUBLIC_KEY)));
		
		return url.toString();
	}

	public String buildURL(String urlService, int offset) {
		StringBuilder url = new StringBuilder(urlService);
		
		Long ts = System.currentTimeMillis();
		
		url.append("?offset=")
		.append(offset)
		.append("&ts=")
		.append(ts)
		.append("&apikey=")
		.append(Base64Utils.decodePhrase(CtsCodeMsg.PUBLIC_KEY))
		.append("&hash=")
		.append(generarHash(ts, Base64Utils.decodePhrase(CtsCodeMsg.PRIVATE_KEY), Base64Utils.decodePhrase(CtsCodeMsg.PUBLIC_KEY)));
		
		return url.toString();
	}

	public String buildURL(String urlService, String firstName, int offset) {
		StringBuilder url = new StringBuilder(urlService);

		Long ts = System.currentTimeMillis();
		
		url.append("?firstName=") 
		.append(firstName)
		.append("&ts=")
		.append(ts)
		.append("&apikey=")
		.append(Base64Utils.decodePhrase(CtsCodeMsg.PUBLIC_KEY))
		.append("&hash=")
		.append(generarHash(ts, Base64Utils.decodePhrase(CtsCodeMsg.PRIVATE_KEY), Base64Utils.decodePhrase(CtsCodeMsg.PUBLIC_KEY)))
		.append("&offset=")
		.append(offset);
		
		return url.toString();
	}
	
	public String generarHash(Long ts, String privateKey, String publicKey) {
		StringBuilder strToHash = new StringBuilder();
		strToHash.append(ts).append(privateKey).append(publicKey);
		
		return DigestUtils.md5DigestAsHex(strToHash.toString().getBytes());
	}

	public boolean validateReqParam(String name, String comics, String series) {
		return (name != null && !name.isBlank()) && (comics != null && !comics.isBlank()) && (series != null && !series.isBlank());
	}

	public boolean validateReqParam(String name) {
		return name != null && !name.isBlank();
	}
	
	public boolean validateReqParam(int id) {
		return id <= 0;
	}

	public String consumerCollection(List<Map<String, Object>> consumerData, String collectionURI, int available) {
		List<Map<String, Object>> coleccion = new ArrayList<Map<String, Object>>();
		
		for (Map<String, Object> data : consumerData) {
			Map<String, Object> item = new HashMap<String, Object>();
			
			item.put("id", data.get("id"));
			item.put("titulo", data.get("titulo"));
			
			coleccion.add(item);
		}
		
		Map<String, Object> infoCollection = new HashMap<String, Object>();
		infoCollection.put("disponibles", available);
		infoCollection.put("urlColeccion", collectionURI);
		infoCollection.put("items", coleccion);
		
		return JSONUtils.convertFromObjectToJson(infoCollection);
	}
}
