package com.test.util;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class WebUtils {
	public <T> ResponseEntity<T> createCustomizedResponse(T body, Integer statusCode, String loggingTrace,
			Long serviceCode, String serviceDescription, Map<String, String> headersMap) {
		HttpStatus httpStatus = HttpStatus.valueOf(statusCode.intValue());
		return createResponse(body, httpStatus, loggingTrace, serviceCode, serviceDescription, headersMap);
	}

	private <T> ResponseEntity<T> createResponse(T body, HttpStatus httpStatus, String loggingTrace, Long serviceCode,
			String serviceDescription, Map<String, String> headersMap) {
		HttpHeaders httpHeaders = createHeadersResponse(httpStatus, loggingTrace, serviceCode, serviceDescription,
				headersMap);
		if (body == null)
			return new ResponseEntity<>(httpHeaders, httpStatus);
		return new ResponseEntity<>(body, httpHeaders, httpStatus);
	}

	private String[] getServiceResult(HttpStatus httpStatus, Long serviceCode, String serviceDescription) {
		String serviceDescriptionStr = ConstantsUtils.CODE_SUCCESS_VALUE;
		String serviceCodeStr = String.valueOf(ConstantsUtils.CODE_SUCCESS_KEY);
		if (serviceCode == null) {
			boolean isError = isError(httpStatus);
			if (isError) {
				serviceCodeStr = String.valueOf(httpStatus.value());
				if (serviceDescription == null) {
					serviceDescriptionStr = ConstantsUtils.CODE_FAILED_VALUE;
				} else {
					serviceDescriptionStr = serviceDescription;
				}
			}
		} else {
			serviceCodeStr = serviceCode.toString();
			serviceDescriptionStr = serviceDescription;
		}
		return new String[] { serviceCodeStr, serviceDescriptionStr };
	}

	private HttpHeaders createHeadersResponse(HttpStatus httpStatus, String loggingTrace, Long serviceCode,
			String serviceDescription, Map<String, String> headersMap) {
		String[] serviceResultArr = getServiceResult(httpStatus, serviceCode, serviceDescription);
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		String xUserAgent = loggingTraceArr[2];
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(new MediaType(MediaType.APPLICATION_JSON, StandardCharsets.UTF_8)));
		headers.set(ConstantsUtils.OPERATION_REF_ID, Transactions.getId());
		headers.set(ConstantsUtils.UNTIL_LAST_SEQUENCE, Transactions.getUntilLastSequence().toString());
		headers.set(ConstantsUtils.X_USER_AGENT_KEY, xUserAgent);
		headers.set(ConstantsUtils.SERVICE_CODE, serviceResultArr[0]);
		headers.set(ConstantsUtils.SERVICE_DESCRIPTION, serviceResultArr[1]);
		if (headersMap != null)
			headers.setAll(headersMap);
		return headers;
	}

	private String[] getLoggingTrace(String loggingTrace) {
		if (loggingTrace == null)
			loggingTrace = "";
		String[] result = loggingTrace.split(ConstantsUtils.LOGGING_TRACE_SPLIT);
		if (result.length != 3) {
			Transactions.setOperationRefId(Transactions.getId());
			Transactions.setSequence(Integer.valueOf(0));
			result = new String[] { "", "",
					String.format(ConstantsUtils.X_USER_AGENT_VALUE, ConstantsUtils.GATEWAY_VALUE) };
		}
		return result;
	}

	public static boolean isError(HttpStatus status) {
		HttpStatus.Series series = status.series();
		return (HttpStatus.Series.CLIENT_ERROR.equals(series) || HttpStatus.Series.SERVER_ERROR.equals(series)
				|| HttpStatus.INTERNAL_SERVER_ERROR.equals(status));
	}

	public String getLoggingTrace(Map<String, String> headersMap) {
		String[] loggingTraceArr = getHeadersRequest(headersMap);
		Transactions.setOperationRefId(loggingTraceArr[0]);
		Transactions.setSequence(Integer.valueOf(Integer.parseInt(loggingTraceArr[1])));
		return createLoggingTrace(Transactions.getId(),
				String.valueOf(Transactions.getUntilLastSequence()), loggingTraceArr[2]);
	}

	private String[] getHeadersRequest(Map<String, String> headersMap) {
		String operationRefId = headersMap.get(ConstantsUtils.OPERATION_REF_ID.toLowerCase());
		String untilLastSequence = headersMap.get(ConstantsUtils.UNTIL_LAST_SEQUENCE.toLowerCase());
		String xUserAgent = headersMap.get(ConstantsUtils.X_USER_AGENT_KEY.toLowerCase());
		return new String[] { operationRefId, untilLastSequence, xUserAgent };
	}

	private String createLoggingTrace(String operationRefId, String untilLastSequence, String gateway) {
		return operationRefId + ":" + untilLastSequence + ":" + gateway;
	}

	public String getHeaderByName(HttpHeaders httpHeaders, String name) {
		String result = null;
		if (httpHeaders == null)
			return result;
		return httpHeaders.toSingleValueMap().get(name);
	}
}
