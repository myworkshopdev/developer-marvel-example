package com.test.notifications;

public class CtsCodeMsg {
	public static final String BASE_URL = "https://gateway.marvel.com:443/v1/public/";
	
	public static final String CHARACTERS_URL = "characters";
	public static final String CHARACTERS_BY_ID_URL = BASE_URL + CHARACTERS_URL + "/%d";
	public static final String CHARACTERS_COMICS_URL = BASE_URL + CHARACTERS_URL + "/%d/comics";
//	public static final String CHARACTERS_EVENT_URL = BASE_URL + CHARACTERS_URL + "/%d/events";
//	public static final String CHARACTERS_STORIES_URL = BASE_URL + CHARACTERS_URL + "/%d/stories";
	public static final String CHARACTERS_SERIES_URL = BASE_URL + CHARACTERS_URL + "/%d/series";
	
	public static final String COMICS_URL = "comics";
	public static final String COMICS_BY_ID_URL = BASE_URL + COMICS_URL + "/%d";
	public static final String COMICS_STORIES_URL = BASE_URL + COMICS_URL + "/%d/stories";
	public static final String COMICS_EVENTS_URL = BASE_URL + COMICS_URL + "/%d/events";
	public static final String COMICS_CREATORS_URL = BASE_URL + COMICS_URL + "/%d/creators";
	public static final String COMICS_CHARACTERS_URL = BASE_URL + COMICS_URL + "/%d/characters";
	
	public static final String CREATORS_URL = BASE_URL + "creators";
	public static final String CREATORS_COMICS_URL = CREATORS_URL + "/%d/comics";
	
	public static final String PUBLIC_KEY = "MDZhMmM0MThhYjBhMTFhNjAwZTk1OWNkMTMyMThmY2I=";
	public static final String PRIVATE_KEY = "ZDZhNDE2NjZhYWJkZGJmOWJiNzEyNzc5Y2FmNjllZWMxOWY1ZTk5Mg==";
	
	// Paquete base para swagger
	public static final String ROUTE_SWAGGER = "com.test.controller";
	
	// Codigos y mensajes de respuesta
	public static final int HTTP_STATUS_CODE_OK_KEY = 200;
	public static final String HTTP_STATUS_CODE_OK_VALUE = "La solicitud ha tenido exito.";
	public static final int HTTP_STATUS_CODE_BADREQUEST_KEY = 400;
	public static final String HTTP_STATUS_CODE_BADREQUEST_VALUE = "El servidor no pudo interpretar la solicitud por sintaxis invalida";
	public static final int HTTP_STATUS_CODE_CONFLICT_KEY = 409;
	public static final String HTTP_STATUS_CODE_CONFLICT_VALUE = "Solicitud con conflicto.";
}
