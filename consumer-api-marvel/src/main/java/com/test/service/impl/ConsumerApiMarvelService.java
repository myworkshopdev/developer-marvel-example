package com.test.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.test.consumer.MarvelConsumer;
import com.test.dto.CharactersMarvelReq;
import com.test.dto.MarvelRsp;
import com.test.entity.Characters;
import com.test.entity.Comics;
import com.test.entity.Creators;
import com.test.exceptions.AppServiceException;
import com.test.exceptions.ErrorMessage;
import com.test.model.Character;
import com.test.model.Comic;
import com.test.model.Creator;
import com.test.notifications.CtsCodeMsg;
import com.test.repository.CharactersRepository;
import com.test.repository.ComicsRepository;
import com.test.repository.CreatorsRepository;
import com.test.service.IConsumerApiMarvelService;
import com.test.util.AppUtil;
import com.test.util.JSONUtils;

@Service
public class ConsumerApiMarvelService implements IConsumerApiMarvelService {
	private static final Logger log = LoggerFactory.getLogger(ConsumerApiMarvelService.class);
	
	@Autowired
	private AppUtil appUtil;
	@Autowired
	private MarvelConsumer marvelConsumer;
	@Autowired
	private CharactersRepository characterRepository;
	@Autowired
	private ComicsRepository comicRepository;
	@Autowired
	private CreatorsRepository creatorRepository;

	@Override
	public MarvelRsp charactersData(String name, String comics, String series) {
		MarvelRsp response = new MarvelRsp();
		
		ErrorMessage er = new ErrorMessage();
		
		Character consumerDataCharacters = new Character();
		Characters characterEntity = new Characters();
		
		try {
			log.info("Personajes de Marvel - {}", this.getClass());
			
			CharactersMarvelReq request = new CharactersMarvelReq();
			
			if (appUtil.validateReqParam(name, comics, series)) {
				request.setName(name);
				request.setComics(comics);
				request.setSeries(series);
			} else {
				er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_KEY));
				er.setErrorMessage(CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_VALUE);
				log.error("Causa del error: {}", er);
				
				throw new AppServiceException(er);
			}
			
			consumerDataCharacters = marvelConsumer.charactersData(request);
			List<Map<String, Object>> consumerDataComics = marvelConsumer.characterComicsData(consumerDataCharacters.getId());
			List<Map<String, Object>> consumerDataSeries = marvelConsumer.seriesData(consumerDataCharacters.getId());
			
			for (int i = 0; i < consumerDataComics.size(); i++) {
				Map<String, Object> item = new HashMap<String, Object>();
				item = consumerDataComics.get(i);
				
				Comics comic = new Comics();
				comic.setId((int) item.get("id"));
				comic.setTitulo((String) item.get("titulo"));
				String creador = JSONUtils.convertFromObjectToJson(item.get("creador"));
				comic.setCreador(creador);
				comic.setDescripcion((String) item.get("descripcion"));
				comic.setCreated_at(new Date());
				comic.setUpdated_at(null);
				
				comicRepository.save(comic);
			}
			
			characterEntity.setId(consumerDataCharacters.getId());
			characterEntity.setNombre(consumerDataCharacters.getName());
			characterEntity.setDescripcion(consumerDataCharacters.getDescription());
			
			String imagen = consumerDataCharacters.getThumbnail().getPath() + "/detail." + consumerDataCharacters.getThumbnail().getExtension();
			characterEntity.setImagen(imagen);
			
			characterEntity.setComics(appUtil.consumerCollection(consumerDataComics, consumerDataCharacters.getComics().getCollectionURI(), consumerDataCharacters.getComics().getAvailable()));
			characterEntity.setSeries(appUtil.consumerCollection(consumerDataSeries, consumerDataCharacters.getSeries().getCollectionURI(), consumerDataCharacters.getSeries().getAvailable()));
			
			characterEntity.setCreated_at(new Date());
			
			characterRepository.save(characterEntity);
			
			response.setCode(String.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY));
			response.setStatus(HttpStatus.OK.name());
			response.setDescription("La base de datos ha sido actualizada");
		} catch (AppServiceException e) {
			throw e;
		}
		
		return response;
	}

	@Override
	public MarvelRsp comicsData(int id) {
		MarvelRsp response = new MarvelRsp();
		
		ErrorMessage er = new ErrorMessage();
		
		Comic consumerDataComics = new Comic();
		Comics comicEntity = new Comics();
		
		try {
			log.info("Comics de Marvel - {}", this.getClass());
			
			if (appUtil.validateReqParam(id)) {
				er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_KEY));
				er.setErrorMessage(CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_VALUE);
				log.error("Causa del error: {}", er);
				
				throw new AppServiceException(er);
			}
			
			consumerDataComics = marvelConsumer.comicsData(id);
			Map<String, Object> consumerDataCreators = marvelConsumer.comicCreatorsData(id);
			
			comicEntity.setId(consumerDataComics.getId());
			comicEntity.setTitulo(consumerDataComics.getTitle());
			comicEntity.setDescripcion(consumerDataComics.getDescription());
			comicEntity.setCreador(JSONUtils.convertFromObjectToJson(consumerDataCreators));
			
			comicEntity.setCreated_at(new Date());
			
			comicRepository.save(comicEntity);
			
			response.setCode(String.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY));
			response.setStatus(HttpStatus.OK.name());
			response.setDescription("La base de datos ha sido actualizada");
		} catch (AppServiceException e) {
			throw e;
		}
		
		return response;
	}

	@Override
	public MarvelRsp creatorsData(String firstName) {
		MarvelRsp response = new MarvelRsp();
		
		ErrorMessage er = new ErrorMessage();
		
		List<Creator> consumerDataCreators = new ArrayList<Creator>();
		Creators creatorEntity = new Creators();
		
		try {
			log.info("Creadores de Marvel - {}", this.getClass());
			
			if (!appUtil.validateReqParam(firstName)) {
				er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_KEY));
				er.setErrorMessage(CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_VALUE);
				log.error("Causa del error: {}", er);
				
				throw new AppServiceException(er);
			}
			
			consumerDataCreators = marvelConsumer.creatorsData(firstName);
			
			for (Creator creatorItem : consumerDataCreators) {
				List<Map<String, Object>> consumerDataComics = marvelConsumer.creatorComicsData(creatorItem.getId());
				
				creatorEntity.setId(creatorItem.getId());
				creatorEntity.setNombre(creatorItem.getFirstName());
				creatorEntity.setApellido(creatorItem.getLastName());
				creatorEntity.setNomCompleto(creatorItem.getFullName());
				creatorEntity.setComics(appUtil.consumerCollection(consumerDataComics, creatorItem.getComics().getCollectionURI(), creatorItem.getComics().getAvailable()));
				
				creatorEntity.setCreated_at(new Date());
				
				creatorRepository.save(creatorEntity);
			}
			response.setCode(String.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY));
			response.setStatus(HttpStatus.OK.name());
			response.setDescription("La base de datos ha sido actualizada");
		} catch (AppServiceException e) {
			throw e;
		}
		
		return response;
	}
}
