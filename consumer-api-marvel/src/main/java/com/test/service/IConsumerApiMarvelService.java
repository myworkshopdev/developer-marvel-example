package com.test.service;

import com.test.dto.MarvelRsp;

public interface IConsumerApiMarvelService {
	public MarvelRsp charactersData(String name, String comics, String series);
	public MarvelRsp comicsData(int id);
	public MarvelRsp creatorsData(String name);
}
