package com.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.entity.Characters;

public interface CharactersRepository extends JpaRepository<Characters, Integer> {

}
