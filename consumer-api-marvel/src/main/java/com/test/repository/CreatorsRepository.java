package com.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.entity.Creators;

public interface CreatorsRepository extends JpaRepository<Creators, Integer> {

}
