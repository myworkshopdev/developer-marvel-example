package com.test.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.test.dto.MarvelRsp;
import com.test.exceptions.AppServiceException;
import com.test.exceptions.ErrorMessage;
import com.test.notifications.CtsCodeMsg;
import com.test.service.IConsumerApiMarvelService;
import com.test.util.Transactions;
import com.test.util.WebUtils;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class ConsumerApiMarvelController {
	private static final Logger log = LoggerFactory.getLogger(ConsumerApiMarvelController.class);

	@Autowired
	private WebUtils webUtil;
	@Autowired
	private IConsumerApiMarvelService service;

	@ApiOperation(value = "Metodo solicita informacion de los personajes de Marvel para almacenarlo en base de datos", response = MarvelRsp.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { 
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, response = MarvelRsp.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_VALUE, response = ErrorMessage.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_VALUE, response = ErrorMessage.class) })
	@GetMapping(value = "/charactersDataAPIMarvel", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> charactersDataAPIMarvel(@RequestHeader HttpHeaders reqHeader, @RequestParam(name = "name", required = true) String name, @RequestParam(name = "comics", required = true) String comics, @RequestParam(name = "series", required = true) String series) {
		ResponseEntity<Object> response = null;
		
		Transactions.begin();
		StopWatch time = new StopWatch();
		
		Map<String, String> headersMap = new HashMap<>();
		
		MarvelRsp rspMarvel = new MarvelRsp();

		try {
			time.start();
			
			log.info("Personajes Marvel - Inicio");
			
			rspMarvel = service.charactersData(name, comics, series);
			// Construyendo la respuesta
			response = webUtil.createCustomizedResponse(rspMarvel, CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, "", Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY), CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, headersMap);
			time.stop();
		} catch (AppServiceException ex) {
			response = webUtil.createCustomizedResponse(ex.getError(), ex.getError().getErrorCode().intValue(), null, ex.getError().getErrorCode(), ex.getError().getErrorMessage(), headersMap);
			time.stop();
		}
		
		log.info("Personajes Marvel - Fin | Tiempo: {}", time.getTotalTimeMillis());

		Transactions.end();
		
		return response;
	}

	@ApiOperation(value = "Metodo solicita informacion de los comics de Marvel por ID para almacenarlo en base de datos", response = MarvelRsp.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { 
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, response = MarvelRsp.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_VALUE, response = ErrorMessage.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_VALUE, response = ErrorMessage.class) })
	@GetMapping(value = "/comicsById", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> comicsById(@RequestHeader HttpHeaders reqHeader, @RequestParam(name = "id", required = true) int id) {
		ResponseEntity<Object> response = null;
		
		Transactions.begin();
		StopWatch time = new StopWatch();
		
		Map<String, String> headersMap = new HashMap<>();
		
		MarvelRsp rspMarvel = new MarvelRsp();

		try {
			time.start();
			
			log.info("ComicsByID Marvel - Inicio");
			
			rspMarvel = service.comicsData(id);
			// Construyendo la respuesta
			response = webUtil.createCustomizedResponse(rspMarvel, CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, "", Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY), CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, headersMap);
			time.stop();
		} catch (AppServiceException ex) {
			response = webUtil.createCustomizedResponse(ex.getError(), ex.getError().getErrorCode().intValue(), null, ex.getError().getErrorCode(), ex.getError().getErrorMessage(), headersMap);
			time.stop();
		}
		
		log.info("ComicsByID Marvel - Fin | Tiempo: {}", time.getTotalTimeMillis());

		Transactions.end();
		
		return response;
	}

	@ApiOperation(value = "Metodo solicita informacion de los creadores de Marvel para almacenarlo en base de datos", response = MarvelRsp.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { 
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, response = MarvelRsp.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_VALUE, response = ErrorMessage.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_VALUE, response = ErrorMessage.class) })
	@GetMapping(value = "/creatorsDataAPIMarvel", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> creatorsDataAPIMarvel(@RequestHeader HttpHeaders reqHeader, @RequestParam(name = "firstName", required = true) String firstName) {
		ResponseEntity<Object> response = null;
		
		Transactions.begin();
		StopWatch time = new StopWatch();
		
		Map<String, String> headersMap = new HashMap<>();
		
		MarvelRsp rspMarvel = new MarvelRsp();

		try {
			time.start();
			
			log.info("Creadores Marvel - Inicio");
			
			rspMarvel = service.creatorsData(firstName);
			// Construyendo la respuesta
			response = webUtil.createCustomizedResponse(rspMarvel, CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, "", Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY), CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, headersMap);
			time.stop();
		} catch (AppServiceException ex) {
			response = webUtil.createCustomizedResponse(ex.getError(), ex.getError().getErrorCode().intValue(), null, ex.getError().getErrorCode(), ex.getError().getErrorMessage(), headersMap);
			time.stop();
		}
		
		log.info("Creadores Marvel - Fin | Tiempo: {}", time.getTotalTimeMillis());

		Transactions.end();
		
		return response;
	}

}
