package com.test.dto;

public class CharactersMarvelReq {
	private String name;
	private String comics;
	private String series;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getComics() {
		return comics;
	}

	public void setComics(String comics) {
		this.comics = comics;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	@Override
	public String toString() {
		return "CharactersMarvelReq [name=" + name + ", comics=" + comics + ", series=" + series + "]";
	}

}
