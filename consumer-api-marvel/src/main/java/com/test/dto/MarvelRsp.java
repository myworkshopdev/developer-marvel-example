package com.test.dto;

public class MarvelRsp {
	private String code;
	private String status;
	private String description;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "CharactersMarvelRsp [code=" + code + ", status=" + status + ", description=" + description + "]";
	}

}
