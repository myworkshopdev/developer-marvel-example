package com.test.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Creators {
	@Id
	private int id;
	private String nombre;
	private String apellido;
	private String nomcompleto;
	private String comics;
	private Date created_at;
	private Date updated_at;

	public Creators() {
		super();
	}

	public Creators(int id, String nombre, String apellido, String nomcompleto, String comics, Date created_at,
			Date updated_at) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.nomcompleto = nomcompleto;
		this.comics = comics;
		this.created_at = created_at;
		this.updated_at = updated_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNomCompleto() {
		return nomcompleto;
	}

	public void setNomCompleto(String nomcompleto) {
		this.nomcompleto = nomcompleto;
	}

	public String getComics() {
		return comics;
	}

	public void setComics(String comics) {
		this.comics = comics;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	@Override
	public String toString() {
		return "Creators [id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", nomCompleto=" + nomcompleto
				+ ", comics=" + comics + ", created_at=" + created_at + ", updated_at=" + updated_at + "]";
	}

}
