package com.test.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Comics {
	@Id
	private int id;
	private String titulo;
	private String descripcion;
	private String creador;
	private Date created_at;
	private Date updated_at;

	public Comics() {
		super();
	}

	public Comics(int id, String titulo, String descripcion, String creador, Date created_at, Date updated_at) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.descripcion = descripcion;
		this.creador = creador;
		this.created_at = created_at;
		this.updated_at = updated_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCreador() {
		return creador;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	@Override
	public String toString() {
		return "Comics [id=" + id + ", titulo=" + titulo + ", descripcion=" + descripcion + ", creador=" + creador
				+ ", created_at=" + created_at + ", updated_at=" + updated_at + "]";
	}

}
