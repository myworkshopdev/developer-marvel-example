package com.test.service;

public interface IInformacionMarvelService {
	public String characterMarvel(String name, int comic, int serie);
	public String comicListByCharacter(String name);
	public String imageDescCharacter(String name);
	public String completeComicList();
	public String comicById(int id);
	public String comicByCreator(String firstName, String lastName);
	public String imageAllCharacters(int page);
}
