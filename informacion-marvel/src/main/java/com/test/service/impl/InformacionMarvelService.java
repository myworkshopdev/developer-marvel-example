package com.test.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.test.consumer.APIConsumer;
import com.test.entity.Characters;
import com.test.entity.Comics;
import com.test.entity.Creators;
import com.test.exceptions.ErrorMessage;
import com.test.notifications.CtsCodeMsg;
import com.test.repository.CharactersRepository;
import com.test.repository.ComicsRepository;
import com.test.repository.CreatorsRepository;
import com.test.service.IInformacionMarvelService;
import com.test.util.AppUtil;
import com.test.util.JSONUtils;

@Service
public class InformacionMarvelService implements IInformacionMarvelService {
	@Autowired
	private AppUtil appUtil;
	@Autowired
	private APIConsumer consumer;
	@Autowired
	private CharactersRepository characterRepository;
	@Autowired
	private ComicsRepository comicRepository;
	@Autowired
	private CreatorsRepository creatorRepository;

	@Override
	public String characterMarvel(String name, int comic, int serie) {
		ErrorMessage er = new ErrorMessage();

		Map<String, String> mapCharacter = new HashMap<String, String>();

		if (appUtil.characterMarvelReq(name, comic, serie)) {
			Characters character = characterRepository.characterByName(name);

			if (character != null) {
				mapCharacter.put("id", String.valueOf(character.getId()));
				mapCharacter.put("nombre", character.getNombre());
				mapCharacter.put("descripcion", character.getDescripcion());
				mapCharacter.put("imagen_url", character.getImagen());

				JSONObject jsonComic = new JSONObject(character.getComics());
				JSONArray itemsComic = jsonComic.getJSONArray("items");

				for (int i = 0; i < itemsComic.length(); i++) {
					JSONObject itemComic = itemsComic.getJSONObject(i);

					if (comic == itemComic.getInt("id")) {
						mapCharacter.put("comic_titulo", itemComic.getString("titulo"));
					}
				}

				JSONObject jsonSerie = new JSONObject(character.getSeries());
				JSONArray itemsSerie = jsonSerie.getJSONArray("items");

				for (int i = 0; i < itemsSerie.length(); i++) {
					JSONObject itemComic = itemsSerie.getJSONObject(i);

					if (comic == itemComic.getInt("id")) {
						mapCharacter.put("serie_titulo", itemComic.getString("titulo"));
					}
				}

				if ((mapCharacter.get("comic_titulo") == null || mapCharacter.get("comic_titulo").isBlank())
						&& (mapCharacter.get("comic_titulo") == null || mapCharacter.get("comic_titulo").isBlank())) {
					er.setECode(Long.valueOf(HttpStatus.NOT_FOUND.value()));
					er.setErrorMessage("IdComic o IdSerie no tiene coincidencias, verifique los datos");

					return JSONUtils.convertFromObjectToJson(er);
				} else {
					return JSONUtils.convertFromObjectToJson(mapCharacter);
				}
			} else {
				String actualizaData = consumer.fillCharactersData(name, String.valueOf(comic), String.valueOf(serie));

				JSONObject rspUpdateData = new JSONObject(actualizaData);

				if (rspUpdateData.getInt("code") == 200) {
					return characterMarvel(name, comic, serie);
				} else {
					er.setECode(Long.valueOf(HttpStatus.NOT_FOUND.value()));
					er.setErrorMessage("Datos proporcionados no son validos");

					return JSONUtils.convertFromObjectToJson(er);
				}
			}
		} else {
			er.setECode(Long.valueOf(HttpStatus.BAD_REQUEST.value()));
			er.setErrorMessage(CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_VALUE);

			return JSONUtils.convertFromObjectToJson(er);
		}
	}

	@Override
	public String comicListByCharacter(String name) {
		ErrorMessage er = new ErrorMessage();

		if (appUtil.characterMarvelReq(name)) {
			Characters character = characterRepository.characterByName(name);

			if (character != null) {
				JSONObject comicList = new JSONObject(character.getComics());
				return comicList.toString(4);
			} else {
				er.setECode(Long.valueOf(HttpStatus.NOT_FOUND.value()));
				er.setErrorMessage("Personaje no existe en registro");

				return JSONUtils.convertFromObjectToJson(er);
			}
		} else {
			er.setECode(Long.valueOf(HttpStatus.BAD_REQUEST.value()));
			er.setErrorMessage(CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_VALUE);

			return JSONUtils.convertFromObjectToJson(er);
		}
	}

	@Override
	public String imageDescCharacter(String name) {
		ErrorMessage er = new ErrorMessage();

		Map<String, String> mapCharacter = new HashMap<String, String>();

		if (appUtil.characterMarvelReq(name)) {
			Characters character = characterRepository.characterByName(name);

			if (character != null) {
				mapCharacter.put("nombre", character.getNombre());
				mapCharacter.put("descripcion", character.getDescripcion());
				mapCharacter.put("imagen_url", character.getImagen());

				return JSONUtils.convertFromObjectToJson(mapCharacter);
			} else {
				er.setECode(Long.valueOf(HttpStatus.NOT_FOUND.value()));
				er.setErrorMessage("Personaje no existe en registro");

				return JSONUtils.convertFromObjectToJson(er);
			}
		} else {
			er.setECode(Long.valueOf(HttpStatus.BAD_REQUEST.value()));
			er.setErrorMessage(CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_VALUE);

			return JSONUtils.convertFromObjectToJson(er);
		}
	}

	@Override
	public String completeComicList() {
		ErrorMessage er = new ErrorMessage();

		JSONArray comics = new JSONArray(comicRepository.findAll());

		if (comics != null && !comics.isEmpty()) {
			return comics.toString(4);
		} else {
			er.setECode(Long.valueOf(HttpStatus.NOT_FOUND.value()));
			er.setErrorMessage("No se encontro respuesta en registro");

			return JSONUtils.convertFromObjectToJson(er);
		}
	}

	@Override
	public String comicById(int id) {
		ErrorMessage er = new ErrorMessage();

		Map<String, String> mapComic = new HashMap<String, String>();

		if (id > 0) {
			Optional<Comics> comics = comicRepository.findById(id);

			if (!comics.isEmpty()) {
				mapComic.put("id", String.valueOf(comics.get().getId()));
				mapComic.put("titulo", comics.get().getTitulo());
				mapComic.put("descripcion", comics.get().getDescripcion());
				mapComic.put("creador", comics.get().getCreador());
				
				return JSONUtils.convertFromObjectToJson(mapComic);
			} else {
				String actualizaData = consumer.fillComicsData(id);

				JSONObject rspUpdateData = new JSONObject(actualizaData);

				if (rspUpdateData.getInt("code") == 200) {
					return comicById(id);
				} else {
					er.setECode(Long.valueOf(HttpStatus.NOT_FOUND.value()));
					er.setErrorMessage("Datos proporcionados no son validos");

					return JSONUtils.convertFromObjectToJson(er);
				}
			}
		} else {
			er.setECode(Long.valueOf(HttpStatus.CONFLICT.value()));
			er.setErrorMessage("ID invalido, intente de nuevo con otro ID");

			return JSONUtils.convertFromObjectToJson(er);
		}
	}

	@Override
	public String comicByCreator(String firstName, String lastName) {
		ErrorMessage er = new ErrorMessage();

		if (appUtil.creatorMarvelReq(firstName, lastName)) {
			StringBuilder fullName = new StringBuilder();
			fullName.append(firstName);
			fullName.append(" ");
			fullName.append(lastName);
			
			Creators creator = creatorRepository.findByFullName(fullName.toString());

			if (creator != null) {
				JSONObject comicList = new JSONObject(creator.getComics());
				
				return comicList.toString(4);
			} else {
				String actualizaData = consumer.fillCreatorsData(firstName);

				JSONObject rspUpdateData = new JSONObject(actualizaData);

				if (rspUpdateData.getInt("code") == 200) {
					return comicByCreator(firstName, lastName);
				} else {
					er.setECode(Long.valueOf(HttpStatus.NOT_FOUND.value()));
					er.setErrorMessage("Datos proporcionados no son validos");

					return JSONUtils.convertFromObjectToJson(er);
				}
			}
		} else {
			er.setECode(Long.valueOf(HttpStatus.CONFLICT.value()));
			er.setErrorMessage("ID invalido, intente de nuevo con otro ID");

			return JSONUtils.convertFromObjectToJson(er);
		}
	}

	@Override
	public String imageAllCharacters(int page) {
		ErrorMessage er = new ErrorMessage();
		List<Map<String, String>> rsp = new ArrayList<Map<String, String>>();

		if (page > 0) {
			Pageable fiveElements = PageRequest.of(page - 1, 5);
			List<Characters> personajes = characterRepository.findAll(fiveElements).toList();

			if (!personajes.isEmpty()) {
				for (Characters character : personajes) {
					Map<String, String> imageAll = new HashMap<String, String>();
					imageAll.put("personaje", character.getNombre());
					imageAll.put("imagen_url", character.getImagen());

					rsp.add(imageAll);
				}

				return JSONUtils.convertFromObjectToJson(rsp);
			} else {
				er.setECode(Long.valueOf(HttpStatus.NO_CONTENT.value()));
				er.setErrorMessage("No se encontraron datos");

				return JSONUtils.convertFromObjectToJson(er);
			}
		} else {
			er.setECode(Long.valueOf(HttpStatus.CONFLICT.value()));
			er.setErrorMessage("Pagina no encontrada, el valor debe ser '1' o mayor");

			return JSONUtils.convertFromObjectToJson(er);
		}
	}

}
