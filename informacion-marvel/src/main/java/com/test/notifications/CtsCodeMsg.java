package com.test.notifications;

public class CtsCodeMsg {
	public static final String BASE_URL = "http://consumer-api-marvel:8080/";
	public static final String DATA_CHARACTER = "charactersDataAPIMarvel";
	public static final String DATA_COMIC = "comicsById";
	public static final String DATA_CREATOR = "creatorsDataAPIMarvel";
	
	// Paquete base para swagger
	public static final String ROUTE_SWAGGER = "com.test.controller";
	
	// Codigos y mensajes de respuesta
	public static final int HTTP_STATUS_CODE_OK_KEY = 200;
	public static final String HTTP_STATUS_CODE_OK_VALUE = "La solicitud ha tenido exito.";
	public static final int HTTP_STATUS_CODE_BADREQUEST_KEY = 400;
	public static final String HTTP_STATUS_CODE_BADREQUEST_VALUE = "El servidor no pudo interpretar la solicitud por sintaxis invalida";
	public static final int HTTP_STATUS_CODE_CONFLICT_KEY = 409;
	public static final String HTTP_STATUS_CODE_CONFLICT_VALUE = "Solicitud con conflicto.";
}
