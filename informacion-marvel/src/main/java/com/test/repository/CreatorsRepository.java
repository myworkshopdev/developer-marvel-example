package com.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.test.entity.Creators;

public interface CreatorsRepository extends JpaRepository<Creators, Integer> {
	@Query("SELECT c FROM Creators c WHERE c.nomcompleto = ?1")
	public Creators findByFullName(String fullName);
}
