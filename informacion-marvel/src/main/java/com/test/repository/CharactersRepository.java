package com.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.test.entity.Characters;

public interface CharactersRepository extends JpaRepository<Characters, Integer> {
	@Query("SELECT c FROM Characters c WHERE c.nombre = lower(?1)")
	public Characters characterByName(String name);
}
