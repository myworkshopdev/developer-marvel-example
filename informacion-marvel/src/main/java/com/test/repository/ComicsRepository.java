package com.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.entity.Comics;

public interface ComicsRepository extends JpaRepository<Comics, Integer> {

}
