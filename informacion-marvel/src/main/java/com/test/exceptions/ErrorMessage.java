package com.test.exceptions;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

public class ErrorMessage implements Serializable {
	private static final long serialVersionUID = -9007883903446706753L;
	
	@ApiModelProperty(notes = "Codigo de error - [TipoDato: Long]")
	private Long eCode;
	@ApiModelProperty(notes = "Mensaje de error - [TipoDato: String]")
	private String eMessage;
	
	public ErrorMessage() {
		super();
	}

	public ErrorMessage(Long eCode, String eMessage) {
		super();
		this.eCode = eCode;
		this.eMessage = eMessage;
	}

	public Long getErrorCode() {
		return eCode;
	}

	public void setECode(Long eCode) {
		this.eCode = eCode;
	}

	public String getErrorMessage() {
		return eMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.eMessage = errorMessage;
	}

	@Override
	public String toString() {
		return "ErrorMesage [eCode=" + eCode + ", eMessage=" + eMessage + "]";
	}

}
