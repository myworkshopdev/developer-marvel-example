package com.test.dto;

import java.util.List;

import com.test.entity.Characters;

public class MarvelRsp {
	private List<Characters> characters;

	public List<Characters> getCharacters() {
		return characters;
	}

	public void setCharacters(List<Characters> characters) {
		this.characters = characters;
	}

	@Override
	public String toString() {
		return "MarvelRsp [characters=" + characters + "]";
	}
	
}
