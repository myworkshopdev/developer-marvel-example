package com.test.consumer;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpClient.Version;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.test.exceptions.AppServiceException;
import com.test.exceptions.ErrorMessage;
import com.test.notifications.CtsCodeMsg;
import com.test.util.AppUtil;

@Component
public class APIConsumer {
	private static final Logger log = LoggerFactory.getLogger(APIConsumer.class);
	private static final HttpClient client = HttpClient.newBuilder().version(Version.HTTP_2).build();
	
	@Autowired
	private AppUtil appUtil;
	
	public String fillCharactersData(String name, String comic, String serie) {
		ErrorMessage er = new ErrorMessage();
		
		try {
			log.info("Actualizando base de datos de personaje Marvel por nombre, comics y series");
			
			String urlService = CtsCodeMsg.BASE_URL + CtsCodeMsg.DATA_CHARACTER;
			HttpRequest httpReq = HttpRequest.newBuilder(URI.create(appUtil.buildURL(urlService, name, comic,serie))).GET().build();
			CompletableFuture<HttpResponse<String>> response = client.sendAsync(httpReq, BodyHandlers.ofString());
			
			if (response.get().statusCode() == 200) {
				return response.get().body();
			} else {
				er.setECode(Long.valueOf(response.get().statusCode()));
				er.setErrorMessage("La consulta de personajes Marvel fallida, datos ingresados no coinciden");
				log.error("Causa del problema: {}", er);
				
				throw new AppServiceException(er);
			}
		} catch (AppServiceException e) {
			throw e;
		} catch (InterruptedException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("InterruptedException: {}", er);
			
			throw new AppServiceException(er);
		} catch (ExecutionException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("ExecutionException: {}", er);
			
			throw new AppServiceException(er);
		}
	}

	public String fillComicsData(int id) {
		ErrorMessage er = new ErrorMessage();
		
		try {
			log.info("Actualizando base de datos de comics Marvel por ID");
			
			String urlService = CtsCodeMsg.BASE_URL + CtsCodeMsg.DATA_COMIC;
			HttpRequest httpReq = HttpRequest.newBuilder(URI.create(appUtil.buildURL(urlService, id))).GET().build();
			CompletableFuture<HttpResponse<String>> response = client.sendAsync(httpReq, BodyHandlers.ofString());
			
			if (response.get().statusCode() == 200) {
				return response.get().body();
			} else {
				er.setECode(Long.valueOf(response.get().statusCode()));
				er.setErrorMessage("La consulta de comics Marvel fallida, ID no valido");
				log.error("Causa del problema: {}", er);
				
				throw new AppServiceException(er);
			}
		} catch (AppServiceException e) {
			throw e;
		} catch (InterruptedException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("InterruptedException: {}", er);
			
			throw new AppServiceException(er);
		} catch (ExecutionException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("ExecutionException: {}", er);
			
			throw new AppServiceException(er);
		}
	}

	public String fillCreatorsData(String firstName) {
ErrorMessage er = new ErrorMessage();
		
		try {
			log.info("Actualizando base de datos de creadores Marvel por firstName");
			
			String urlService = CtsCodeMsg.BASE_URL + CtsCodeMsg.DATA_CREATOR;
			HttpRequest httpReq = HttpRequest.newBuilder(URI.create(appUtil.buildURL(urlService, firstName))).GET().build();
			CompletableFuture<HttpResponse<String>> response = client.sendAsync(httpReq, BodyHandlers.ofString());
			
			if (response.get().statusCode() == 200) {
				return response.get().body();
			} else {
				er.setECode(Long.valueOf(response.get().statusCode()));
				er.setErrorMessage("La consulta de comics Marvel fallida, ID no valido");
				log.error("Causa del problema: {}", er);
				
				throw new AppServiceException(er);
			}
		} catch (AppServiceException e) {
			throw e;
		} catch (InterruptedException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("InterruptedException: {}", er);
			
			throw new AppServiceException(er);
		} catch (ExecutionException e) {
			er.setECode(Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY));
			er.setErrorMessage(e.getMessage());
			log.error("ExecutionException: {}", er);
			
			throw new AppServiceException(er);
		}
	}
}
