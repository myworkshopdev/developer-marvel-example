package com.test.util;

import org.springframework.stereotype.Component;

@Component
public class AppUtil {

	public boolean characterMarvelReq(String name, int comic, int serie) {
		return (name != null && !name.isBlank()) && (comic > 0) && (serie > 0);
	}

	public boolean characterMarvelReq(String name) {
		return (name != null && !name.isBlank());
	}

	public boolean creatorMarvelReq(String firstName, String lastName) {
		return (firstName != null && !firstName.isBlank()) && (lastName != null && !lastName.isBlank());
	}

	public String buildURL(String urlService, String name, String comic, String serie) {
		StringBuilder url = new StringBuilder(urlService);
		
		url.append("?name=") 
		.append(name.replace(" ", "%20"))
		.append("&comics=")
		.append(comic)
		.append("&series=")
		.append(serie);
		
		return url.toString();
	}

	public String buildURL(String urlService, int id) {
		StringBuilder url = new StringBuilder(urlService);
		
		url.append("?id=") 
		.append(id);
		
		return url.toString();
	}

	public String buildURL(String urlService, String firstName) {
		StringBuilder url = new StringBuilder(urlService);
		
		url.append("?firstName=") 
		.append(firstName);
		
		return url.toString();
	}

}
