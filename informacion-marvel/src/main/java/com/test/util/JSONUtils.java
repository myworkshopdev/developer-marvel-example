package com.test.util;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONUtils {
	private static final Logger log = LoggerFactory.getLogger(JSONUtils.class);
	
	private JSONUtils() {}
	
	// Convertir un JSON en una lista de objetos
	public static <T> List<T> convertFromJsonToList(String json, TypeReference<List<T>> typeRef) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(json, typeRef);
		} catch (JsonProcessingException e) {
			log.error("JSONUtils Exception - convertFromJsonToList: {}", e.getMessage());
			return Collections.<T>emptyList();
		}
	}
	
	// Metodo seguro de tipo generico: convierte JSON en objeto
	public static <T> T convertFromJsonToObject(String json, Class<T> typeRef) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(json, typeRef);			// Convertir Json en un objeto de tipo especifico
		} catch (JsonProcessingException e) {
			log.error("JSONUtils Exception - convertFromJsonToObject: {}", e.getMessage());
			return null;
		}
    }

	// Convierte un objeto en un JSON
    public static String convertFromObjectToJson(Object obj) {
        try {
        	ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			log.error("JSONUtils Exception - convertFromObjectToJson: {}", e.getMessage());
			return null;
		}
    }

}
