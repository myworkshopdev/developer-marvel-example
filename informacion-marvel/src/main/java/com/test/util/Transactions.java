package com.test.util;

import java.util.UUID;
import java.util.function.Supplier;

public class Transactions {
	private static ThreadLocal<Integer> sequenceId = new ThreadLocal<>();
	private static ThreadLocal<String> target = new ThreadLocal<>();
	private static ThreadLocal<String> msisdn = new ThreadLocal<>();
	
	private static Supplier<String> supplier = () -> Transactions.generateId();
	private static ThreadLocal<String> threadLocal = ThreadLocal.withInitial(supplier);
	
	private Transactions() {}

	public static String getTarget() {
		return target.get();
	}

	public static void setTarget(String target) {
		Transactions.target.set(target);
	}

	public static String getMsisdn() {
		return msisdn.get();
	}

	public static void setMsisdn(String msisdn) {
		Transactions.msisdn.set(msisdn);
	}

	public static Integer getSequence() {
		return sequenceId.get();
	}

	public static void setSequence(Integer seq) {
		sequenceId.set(seq);
	}

	public static String nextSequenceId() {
		int sequence = getSequence().intValue();
		setSequence(Integer.valueOf(++sequence));
		return String.valueOf(getSequence());
	}

	public static void begin() {
		setMsisdn("");
		setTarget("");
		setSequence(Integer.valueOf(0));
		threadLocal.set(generateId());
	}

	public static String getId() {
		return threadLocal.get();
	}

	public static void end() {
		threadLocal.remove();
		sequenceId.remove();
		target.remove();
		msisdn.remove();
	}

	public static String generateId() {
		return UUID.randomUUID().toString();
	}

	public static void setOperationRefId(String operationRefId) {
		threadLocal.set(operationRefId);
	}

	public static Integer getUntilLastSequence() {
		return getSequence();
	}
}
