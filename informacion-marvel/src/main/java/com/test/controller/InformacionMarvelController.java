package com.test.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.test.exceptions.AppServiceException;
import com.test.exceptions.ErrorMessage;
import com.test.notifications.CtsCodeMsg;
import com.test.service.IInformacionMarvelService;
import com.test.util.Transactions;
import com.test.util.WebUtils;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class InformacionMarvelController {
	private static final Logger log = LoggerFactory.getLogger(InformacionMarvelController.class);

	@Autowired
	private WebUtils webUtil;
	@Autowired
	private IInformacionMarvelService service;

	@ApiOperation(value = "Obtiene el personaje por nombre, comic y serie", response = String.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { 
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, response = String.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_VALUE, response = ErrorMessage.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_VALUE, response = ErrorMessage.class) })
	@GetMapping(value = "/characterByNameComicSerie", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> characterByNameComicSerie(@RequestHeader HttpHeaders reqHeader, @RequestParam(name = "name", required = true) String name, @RequestParam(name = "comic", required = true) int comic, @RequestParam(name = "serie", required = true) int serie) {
		ResponseEntity<Object> response = null;
		
		Transactions.begin();
		StopWatch time = new StopWatch();
		
		Map<String, String> headersMap = new HashMap<>();
		headersMap.put("Accept", "*/*");
		
		try {
			time.start();
			
			log.info("Personajes por nombre, comic y serie - Inicio");

			response = webUtil.createCustomizedResponse(service.characterMarvel(name, comic, serie), CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, "", Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY), CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, headersMap);
			time.stop();
		} catch (AppServiceException ex) {
			response = webUtil.createCustomizedResponse(ex.getError(), ex.getError().getErrorCode().intValue(), null, ex.getError().getErrorCode(), ex.getError().getErrorMessage(), headersMap);
			time.stop();
		}
		
		log.info("Personajes por nombre, comic y serie - Fin | Tiempo: {}", time.getTotalTimeMillis());

		Transactions.end();
		
		return response;
	}

	@ApiOperation(value = "Obtiene el listado de comics que tiene un personaje específico", response = String.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { 
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, response = String.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_VALUE, response = ErrorMessage.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_VALUE, response = ErrorMessage.class) })
	@GetMapping(value = "/comicListByCharacterName", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> comicListByCharacterName(@RequestHeader HttpHeaders reqHeader, @RequestParam(name = "name", required = true) String name) {
		ResponseEntity<Object> response = null;
		
		Transactions.begin();
		StopWatch time = new StopWatch();
		
		Map<String, String> headersMap = new HashMap<>();
		headersMap.put("Accept", "*/*");
		
		try {
			time.start();
			
			log.info("Lista de comics por personaje - Inicio");

			response = webUtil.createCustomizedResponse(service.comicListByCharacter(name), CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, "", Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY), CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, headersMap);
			time.stop();
		} catch (AppServiceException ex) {
			response = webUtil.createCustomizedResponse(ex.getError(), ex.getError().getErrorCode().intValue(), null, ex.getError().getErrorCode(), ex.getError().getErrorMessage(), headersMap);
			time.stop();
		}
		
		log.info("Lista de comics por personaje - Fin | Tiempo: {}", time.getTotalTimeMillis());

		Transactions.end();
		
		return response;
	}

	@ApiOperation(value = "Obtiene imagen y descripcion de un personaje específico", response = String.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { 
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, response = String.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_VALUE, response = ErrorMessage.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_VALUE, response = ErrorMessage.class) })
	@GetMapping(value = "/imageDescByCharacterName", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> imageDescByCharacterName(@RequestHeader HttpHeaders reqHeader, @RequestParam(name = "name", required = true) String name) {
		ResponseEntity<Object> response = null;
		
		Transactions.begin();
		StopWatch time = new StopWatch();
		
		Map<String, String> headersMap = new HashMap<>();
		headersMap.put("Accept", "*/*");
		
		try {
			time.start();
			
			log.info("Imagen y descripcion del personaje - Inicio");

			response = webUtil.createCustomizedResponse(service.imageDescCharacter(name), CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, "", Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY), CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, headersMap);
			time.stop();
		} catch (AppServiceException ex) {
			response = webUtil.createCustomizedResponse(ex.getError(), ex.getError().getErrorCode().intValue(), null, ex.getError().getErrorCode(), ex.getError().getErrorMessage(), headersMap);
			time.stop();
		}
		
		log.info("Imagen y descripcion del personaje - Fin | Tiempo: {}", time.getTotalTimeMillis());

		Transactions.end();
		
		return response;
	}

	@ApiOperation(value = "Obtiene listas de comics completas", response = String.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { 
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, response = String.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_VALUE, response = ErrorMessage.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_VALUE, response = ErrorMessage.class) })
	@GetMapping(value = "/comics", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> comics(@RequestHeader HttpHeaders reqHeader) {
		ResponseEntity<Object> response = null;
		
		Transactions.begin();
		StopWatch time = new StopWatch();
		
		Map<String, String> headersMap = new HashMap<>();
		headersMap.put("Accept", "*/*");
		
		try {
			time.start();
			
			log.info("All comics - Inicio");

			response = webUtil.createCustomizedResponse(service.completeComicList(), CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, "", Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY), CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, headersMap);
			time.stop();
		} catch (AppServiceException ex) {
			response = webUtil.createCustomizedResponse(ex.getError(), ex.getError().getErrorCode().intValue(), null, ex.getError().getErrorCode(), ex.getError().getErrorMessage(), headersMap);
			time.stop();
		}
		
		log.info("All comics - Fin | Tiempo: {}", time.getTotalTimeMillis());

		Transactions.end();
		
		return response;
	}

	@ApiOperation(value = "Obtiene comic por id", response = String.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { 
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, response = String.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_VALUE, response = ErrorMessage.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_VALUE, response = ErrorMessage.class) })
	@GetMapping(value = "/comicsById", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> comicsById(@RequestHeader HttpHeaders reqHeader, @RequestParam(name = "comic", required = true) int id) {
		ResponseEntity<Object> response = null;
		
		Transactions.begin();
		StopWatch time = new StopWatch();
		
		Map<String, String> headersMap = new HashMap<>();
		headersMap.put("Accept", "*/*");
		
		try {
			time.start();
			
			log.info("Comics por ID - Inicio");

			response = webUtil.createCustomizedResponse(service.comicById(id), CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, "", Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY), CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, headersMap);
			time.stop();
		} catch (AppServiceException ex) {
			response = webUtil.createCustomizedResponse(ex.getError(), ex.getError().getErrorCode().intValue(), null, ex.getError().getErrorCode(), ex.getError().getErrorMessage(), headersMap);
			time.stop();
		}
		
		log.info("Comics por ID - Fin | Tiempo: {}", time.getTotalTimeMillis());

		Transactions.end();
		
		return response;
	}

	@ApiOperation(value = "Obtiene comic por creador", response = String.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { 
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, response = String.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_VALUE, response = ErrorMessage.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_VALUE, response = ErrorMessage.class) })
	@GetMapping(value = "/comicListByCreator", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> comicListByCreator(@RequestHeader HttpHeaders reqHeader, @RequestParam(name = "firstName", required = true) String firstName, @RequestParam(name = "lastName", required = true) String lastName) {
		ResponseEntity<Object> response = null;
		
		Transactions.begin();
		StopWatch time = new StopWatch();
		
		Map<String, String> headersMap = new HashMap<>();
		headersMap.put("Accept", "*/*");
		
		try {
			time.start();
			
			log.info("Comics por creador - Inicio");

			response = webUtil.createCustomizedResponse(service.comicByCreator(firstName, lastName), CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, "", Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY), CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, headersMap);
			time.stop();
		} catch (AppServiceException ex) {
			response = webUtil.createCustomizedResponse(ex.getError(), ex.getError().getErrorCode().intValue(), null, ex.getError().getErrorCode(), ex.getError().getErrorMessage(), headersMap);
			time.stop();
		}
		
		log.info("Comics por creador - Fin | Tiempo: {}", time.getTotalTimeMillis());

		Transactions.end();
		
		return response;
	}

	@ApiOperation(value = "Obtiene las imagenes de todos los personajes con paginados de 5", response = String.class, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { 
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, response = String.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_BADREQUEST_VALUE, response = ErrorMessage.class),
			@ApiResponse(code = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_KEY, message = CtsCodeMsg.HTTP_STATUS_CODE_CONFLICT_VALUE, response = ErrorMessage.class) })
	@GetMapping(value = "/imageAllCharacters", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> imageAllCharacters(@RequestHeader HttpHeaders reqHeader, @RequestParam(name = "page", required = true) int page) {
		ResponseEntity<Object> response = null;
		
		Transactions.begin();
		StopWatch time = new StopWatch();
		
		Map<String, String> headersMap = new HashMap<>();
		headersMap.put("Accept", "*/*");
		
		try {
			time.start();
			
			log.info("Imagenes personajes Marvel - Inicio");

			response = webUtil.createCustomizedResponse(service.imageAllCharacters(page), CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY, "", Long.valueOf(CtsCodeMsg.HTTP_STATUS_CODE_OK_KEY), CtsCodeMsg.HTTP_STATUS_CODE_OK_VALUE, headersMap);
			time.stop();
		} catch (AppServiceException ex) {
			response = webUtil.createCustomizedResponse(ex.getError(), ex.getError().getErrorCode().intValue(), null, ex.getError().getErrorCode(), ex.getError().getErrorMessage(), headersMap);
			time.stop();
		}
		
		log.info("Imagenes personajes Marvel - Fin | Tiempo: {}", time.getTotalTimeMillis());

		Transactions.end();
		
		return response;
	}

}
